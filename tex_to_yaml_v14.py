import argparse
import numpy as np

class uncertainty:
    #Uncertainty class to contain the magnitude and type
    def __init__(self, unc, unc_type):
        self.unc = unc
        self.unc_type = unc_type
    def __repr__(self):
        if self.unc_type == "e": #Normal symmetric uncertainty denoted by "e"
            return "    - {symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "stat": #Statistical uncertainty denoted by "stat"
            return "    - {label: stat, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "sys": #Systematic uncertainty denoted by "sys"
            return "    - {label: sys, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "lumi": #Luminosity uncertainty denoted by "lumi"
            return "    - {label: 'sys,lumi', symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "asym": #Asymmetric error denoted by "asym", list of form [plus_error, minus_error]
            return "    - {asymerror: {plus: " + str(self.unc[0]) + ", minus: -" + str(self.unc[1]) + "}}\n"
        else: #Also allows for custom uncertainty label
            return "    - {label: '" + self.unc_type + "', symerror: " + str(self.unc) + "}\n"

class value:
    #Value class to contain a value and a list of its uncertainties
    def __init__(self, x):
        self.x = x
        self.uncertainties = []
    def append(self, unc): #Overwrites append to add an uncertainty object to the list
        self.uncertainties.append(unc)
    def __repr__(self):
        if len(self.uncertainties) > 0:
            output =  "  - errors:\n"
            for i in self.uncertainties:
                output += repr(i)
            output += "    value: " + str(self.x) + "\n"
        else:
            output = "  - value: " + str(self.x) + "\n"
        return output
        
class variable:
    #Variable class to contain the column header and each value the variable takes, with uncertainties
    def __init__(self, is_indep, header, qualifiers):
        self.is_indep = is_indep
        self.header = header
        self.qualifiers = qualifiers #qualifiers should be the name of a file
        self.values = []
    def append(self, val): #Overwrites apppend to add value objects to values
        self.values.append(val)
    def __repr__(self):
        output = "- header: {" + self.header + "}\n"
        if self.qualifiers != "None":
            output += "  "
            qualifiers_file = open(self.qualifiers, "r")
            output += qualifiers_file.read()
            output += "\n"
        output += "  values:\n"
        for i in self.values:
            output += repr(i)
        return output

def discard(line):
    #Discards lines in tex file that begin with % or \, but not if it begins with \multiline
    line = line.strip(" \n\t~{}")
    if line == "":
        return True
    elif line.startswith("\multicolumn"):
        return False
    elif line[0] == "%" or line[0] == "\\":
        return True
    else:
        return False

def extract_header(string):
    #Extracts the name and units (if any) from the header of a column and returns a string suitable for yaml
    string = string.replace("\\xspace", "").strip(" \n\t~\\")
    if string.startswith("multicolumn"): #Removes leading multicolumn expression from header
        string = string[string.find("}")+1:]
        string = string[string.find("}")+1:]
    string.strip("{}")

    if "[" in string and "]" in string: #Takes expression in square brackets to be the units
        units = string[string.find("[")+1:string.find("]")]
        string = string[:string.find("[")]
        return "name: '" + string.strip(" \n\t~\\") + "', units: " + units.strip(" \n\t~\\")
    else:
        return "name: '" + string.strip(" \n\t~\\") + "'"

def split_format(string): #Splits format string so that each variable is its own string in a list
    temp_list = []
    for i in string.split(" "):
        if i == "i" or i == "d":
            temp_list.append([i])
        else:
            temp_list[-1].append(i)
    return temp_list

def extract_value(string, format_fragment): #Takes string with errors separated by commas and returns value object
    if string.replace("\t", "").replace(" ", "") == "" or string.replace("\t", "").replace(" ", "") == "-":
        return value("'-'")
    else:
        temp_list = string.split("\pm")
        temp_value = value(temp_list[0].strip(" \n\t~\\$!"))
        for k in range(1,len(temp_list)):
            if format_fragment[k] == "asym":
                temp_value.append(uncertainty([temp_list[k][temp_list[k].find("+"):temp_list[k].find("-")], temp_list[k][temp_list[k].find("-")+1:]], format_fragment[k]))
            temp_value.append(uncertainty(temp_list[k].strip(" \n\t~\\$!"), format_fragment[k]))
        return temp_value

def write_variables(variable_list, file): #Writes variable objects in list to a yaml file
    file.write("dependent_variables:\n")
    for i in variable_list:
        if i.is_indep == False:
            file.write(repr(i))
    file.write("independent_variables:\n")
    for i in variable_list:
        if i.is_indep == True:
            file.write(repr(i))

def remove_phantoms(string):
    while "\phantom" in string:
        x = string.find("\phantom")
        while string[x] != "}":
            x += 1
        string = string[:string.find("\phantom")] + string[x+1:]
    return string

def asym_unc(string):
    string = string.replace(" ", "").replace("^", "").replace("_", "").replace("{", "").replace("}", "")
    return uncertainty([string[string.find("+")+1:string.find("-")], string[string.find("-")+1:]], "asym")

parser = argparse.ArgumentParser()
parser.add_argument("-file", type=str) #-file contains name of data file
parser.add_argument("-format", type=str) #-format contains string of data format (not necessary for covariance)
parser.add_argument("--covariance", action="store_true", default=False) #Flag to say whether the data is a covariance matrix or not
parser.add_argument("-qualifiers", type=str, default="None") #-qualifiers contains name of qualifiers file, defaults to "None"

args = parser.parse_args()

input_file = open(args.file, "r")
lines = input_file.readlines() #Stores each line of the file as a string in in the lines list
input_file.close()

data_array = []
for i in lines:
    if discard(i) == False: #Only valid lines
        if args.covariance == True:
            data_array.append(remove_phantoms(i).replace("\\hline", "").strip(" \n\t~\\").split("&"))
        else:
            if len(data_array) == 0:
                data_array.append(remove_phantoms(i).replace("\\hline", "").strip(" \n\t~\\").split("&"))
            else:
                line_split = remove_phantoms(i).replace("\\hline", "").strip(" \n\t~\\$").split("&")
                if "\pm" in line_split:
                    line_split.remove("\pm")
                if " \pm " in line_split:
                    line_split.remove(" \pm ")
                count = 0
                value_list = []
                for j in line_split:
                    temp = j.split("\pm")
                    temp = [x.strip(" \n\t~\\$") for x in temp]
                    if len(temp) > 1:
                        if "" in temp:
                            temp.remove("")
                        if " " in temp:
                            temp.remove(" ")
                    if args.format.split(" ")[count] == "i" or args.format.split(" ")[count] == "d":
                        value_list.append(value(temp[0].strip(" \n\t~\\$!")))
                        count += 1
                        for k in range(1, len(temp)):
                            if args.format.split(" ")[count] == "asym":
                                value_list[-1].append(asym_unc(temp[k].strip(" \n\t~\\$!")))
                            else:
                                value_list[-1].append(uncertainty(temp[k].strip(" \n\t~\\$!"), args.format.split(" ")[count]))
                            count += 1
                    else:
                        if args.format.split(" ")[count] == "asym":
                            value_list[-1].append(asym_unc(temp[0].strip(" \n\t~\\$!")))
                        else:
                            value_list[-1].append(uncertainty(temp[0].strip(" \n\t~\\$!"), args.format.split(" ")[count]))
                        count += 1
                data_array.append(value_list)
        
data_array = np.stack(data_array, axis=0) #Converts to 2D numpy array

if args.covariance == True:
    for i in range(len(data_array)):
        for j in range(len(data_array[0])):
            if data_array[i,j].strip(" \t-") == "":
                data_array[i,j] = data_array[j,i]
    indep_header = extract_header(data_array[0,0]) 
    variables = [variable(True, indep_header, "None"), variable(True, indep_header, "None"), variable(False, "name: Correlation", args.qualifiers)]
    x_axis = []
    y_axis = []
    for i in data_array[0, 1:]:
        x_axis.append(value(i.strip(" \n\t~\\$")))
    for i in data_array[1:, 0]:
        y_axis.append(value(i.strip(" \n\t~\\$")))
    for i in range(len(x_axis)):
        for j in range(len(y_axis)):
            variables[0].append(x_axis[j])
            variables[1].append(y_axis[i])

    dep_values = data_array[1:,1:].flatten()
    for i in dep_values:
        variables[2].append(value(i.strip(" \n\t~\\$")))
else:
    variables = []
    format_list = split_format(args.format)
    for i in range(len(format_list)):
        if format_list[i][0] == "i":
            variables.append(variable(True, extract_header(data_array[0,i]), "None"))
        elif format_list[i][0] == "d":
            variables.append(variable(False, extract_header(data_array[0,i]), args.qualifiers))
    for i in range(1,len(data_array)):
        for j in range(len(data_array[i,:])):
            variables[j].append(data_array[i,j])
            
output_file = open(args.file.replace(".tex", ".yaml"), "w")
write_variables(variables, output_file)
output_file.close()   

                          
