class uncertainty:
    def __init__(self, unc, unc_type):
        self.unc = unc
        self.unc_type = unc_type
    def __repr__(self):
        if self.unc_type == "none":
            return "    - {symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "stat":
            return "    - {label: stat, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "sys":
            return "    - {label: sys, symerror: " + str(self.unc) + "}\n"
        else:
            return "    - {label: '" + self.unc_type + "', symerror: " + str(self.unc) + "}\n"

class value:
    def __init__(self, x):
        self.x = x
        self.uncertainties = []
    def append(self, unc):
        self.uncertainties.append(unc)
    def __repr__(self):
        output = ""
        if len(self.uncertainties) > 0:
            output +=  "  - errors:\n"
            for i in self.uncertainties:
                output += repr(i)
        output += "    value: " + str(self.x) + "\n"
        return output
        
class variable:
    def __init__(self, is_indep, header):
        self.is_indep = is_indep
        self.header = header
        self.values = []
    def append(self, val):
        self.values.append(val)
    def __repr__(self):
        output = "header: {" + self.header + "}\n  values:\n"
        for i in self.values:
            output += repr(i)
        return output

def remove_spaces(string):
    #Removes leading and ending blank spaces
    while string.startswith(" "):
        string = string[1:]
    while string.endswith(" "):
        string = string[:-1]
    return string

def remove_linebreak(string):
    #Removes ending \\, \n or \hline from strings
    if string.endswith("\\\n"): #Removes ending \\
        return string[:-3]
    elif i.endswith("\\\\\\hline\n"): #Removes ending \\\hline
        return string[:-10]
    elif i.endswith("\n"): #Removes ending linebreak character
        return string[:-2]
    else:
        return string

def discard(line):
    #Discards lines in tex file that begin with % or \, but not if it begins with \multiline
    line = remove_spaces(line)
    if line.startswith("\multiline"):
        return False
    elif line[0] == "%" or line[0] == "\\":
        return True
    else:
        return False

def extract_header(string):
    #Extracts the name and units (if any) from the header of a column and returns a string suitable for yaml
    string = remove_spaces(string)
    if string.startswith("\multicolumn"):
        string = string[string.find("}")+1:]
        string = string[string.find("}")+1:]
    if string.startswith("{") and string.endswith("}"):
        string = string[1:-1]

    if "[" in string and "]" in string:
        units = string[string.find("[")+1:string.find("]")]
        string = string[:string.find("[")]
        return "name: '" + remove_spaces(string) + "', units: " + remove_spaces(units)
    else:
        return "name: '" + remove_spaces(string) + "'"

def is_independent(header):
    answer = input("Is " + header + " an independent or dependent variable? Please enter [I/D]: ")
    while answer != "I" and answer != "D":
        answer = input("Is " + header + " an independent or dependent variable? Please enter [I/D]: ")
    if answer == "I":
        return True
    else:
        return False

input_file = open("table.tex", "r")
lines = input_file.readlines()
input_file.close()

data_lines = [] #Stores lines in tex file that contain relevant information
for i in lines:
    if discard(i) == False:
        data_lines.append(remove_linebreak(remove_spaces(i)))
        print(data_lines[-1])

variables = []
header_line_split = data_lines.pop(0).split("&") #Removes the first line containing header information from data_lines and splits by &
print("The program has detected " + str(len(header_line_split)) + " variables.")
for i in header_line_split:
    header = extract_header(i)
    if is_independent(header):
        variables.append(variable(True, header))
    else:
        variables.append(variable(False, header))

for i in data_lines:
    line_fragments = i.replace("&$\pm$&", ",").split("&")
    for j in range(len(line_fragments)):
        temp_list = line_fragments[j].split(",")
        temp_value = value(temp_list[0])
        if len(temp_list) > 1:
            for k in range(1, len(temp_list)):
                temp_value.append(uncertainty(temp_list[k], "none"))

        print(temp_value)
        variables[j].append(temp_value)

output_file = open("table2.yaml", "w")
output_file.write("dependent_variables:\n")
for i in variables:
    if i.is_indep == False:
        output_file.write(repr(i))
output_file.write("independent_variables:\n")
for i in variables:
    if i.is_indep == True:
        output_file.write(repr(i))
output_file.close()


