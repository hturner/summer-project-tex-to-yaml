class uncertainty:
    def __init__(self, unc, unc_type):
        self.unc = unc
        self.unc_type = unc_type
    def __repr__(self):
        if self.unc_type == "none":
            return "    - {symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "stat":
            return "    - {label: stat, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "sys":
            return "    - {label: sys, symerror: " + str(self.unc) + "}\n"
        else:
            return "    - {label: '" + self.unc_type + "', symerror: " + str(self.unc) + "}\n"

class value:
    def __init__(self, x):
        self.x = x
        self.uncertainties = []
    def append(self, unc):
        self.uncertainties.append(unc)
    def __repr__(self):
        output = ""
        if len(self.uncertainties) > 0:
            output +=  "  - errors:\n"
            for i in self.uncertainties:
                output += repr(i)
        output += "    value: " + str(self.x) + "\n"
        return output
        
class variable:
    def __init__(self, is_indep, header):
        self.is_indep = is_indep
        self.header = header
        self.values = []
    def append(self, val):
        self.values.append(val)
    def __repr__(self):
        output = "header: {" + self.header + "}\n  values:\n"
        for i in self.values:
            output += repr(i)
        return output

def discard(line):
    #Discards lines in tex file that begin with % or \, but not if it begins with \multiline
    line = line.strip(" \n\t~")
    if line == "":
        return True
    elif line.startswith("\multicolumn"):
        return False
    elif line[0] == "%" or line[0] == "\\":
        return True
    else:
        return False

def extract_header(string):
    #Extracts the name and units (if any) from the header of a column and returns a string suitable for yaml
    string = string.strip(" \n\t~\\")
    if string.startswith("multicolumn"):
        string = string[string.find("}")+1:]
        string = string[string.find("}")+1:]
    if string.startswith("{") and string.endswith("}"):
        string = string[1:-1]

    if "[" in string and "]" in string:
        units = string[string.find("[")+1:string.find("]")]
        string = string[:string.find("[")]
        return "name: '" + string.strip(" \n\t~\\") + "', units: " + units.strip(" \n\t~\\")
    else:
        return "name: '" + string.strip(" \n\t~\\") + "'"

def is_independent(header):
    answer = input("Is " + header + " an independent or dependent variable? Please enter [I/D]: ")
    while answer != "I" and answer != "D":
        answer = input("Is " + header + " an independent or dependent variable? Please enter [I/D]: ")
    if answer == "I":
        return True
    else:
        return False

def extract_variables(data):
    variables = []
    header_line_split = data.pop(0).split("&") #Removes the first line containing header information from data_lines and splits by &
    print("The program has detected " + str(len(header_line_split)) + " variables.")
    for i in header_line_split:
        header = extract_header(i)
        if is_independent(header):
            variables.append(variable(True, header))
        else:
            variables.append(variable(False, header))

    for i in data:
        line_fragments = i.replace("&$\pm$&", ",").replace("\pm",",").split("&")
        for j in range(len(line_fragments)):
            temp_list = line_fragments[j].split(",")
            temp_value = value(temp_list[0].strip(" \n\t~\\$!"))
            if len(temp_list) > 1:
                for k in range(1, len(temp_list)):
                    temp_value.append(uncertainty(temp_list[k].strip(" \n\t~\\$!"), "none"))
            #print(temp_value)
            variables[j].append(temp_value)
    return variables

def write_variables(variable_list, file):
    file.write("dependent_variables:\n")
    for i in variable_list:
        if i.is_indep == False:
            file.write(repr(i))
    file.write("independent_variables:\n")
    for i in variable_list:
        if i.is_indep == True:
            file.write(repr(i))

input_file = open("table2.tex", "r")
lines = input_file.readlines()
input_file.close()

header_rows = [] #Stores the numbers of rows that are headers
data_lines = [] #Stores lines in tex file that contain relevant information
for i in lines:
    if discard(i) == False:
        data_lines.append(i.replace("\\hline", "").strip(" \n\t~\\"))
        if "multicolumn" in i:
            print("SIIII")
            header_rows.append(len(data_lines))
        print(data_lines[-1])
header_rows.append(len(data_lines)+1)

output_file = open("table2.yaml", "w")
for i in range(len(header_rows)-1):
    write_variables(extract_variables(data_lines[header_rows[i]:header_rows[i+1]-1]), output_file)
    if i != len(header_rows)-2:
        output_file.write("---\n")
output_file.close()



