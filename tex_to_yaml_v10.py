import argparse
import numpy as np

class uncertainty:
    #Uncertainty class to contain the magnitude and type
    def __init__(self, unc, unc_type):
        self.unc = unc
        self.unc_type = unc_type
    def __repr__(self):
        if self.unc_type == "e":
            return "    - {symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "stat":
            return "    - {label: stat, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "sys":
            return "    - {label: sys, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "lumi":
            return "    - {label: 'sys,lumi', symerror: " + str(self.unc) + "}\n"
        else:
            return "    - {label: '" + self.unc_type + "', symerror: " + str(self.unc) + "}\n"

class value:
    #Value class to contain a value and a list of its uncertainties
    def __init__(self, x):
        self.x = x
        self.uncertainties = []
    def append(self, unc): #Overwrites append to add an uncertainty object to the list
        self.uncertainties.append(unc)
    def __repr__(self):
        if len(self.uncertainties) > 0:
            output =  "  - errors:\n"
            for i in self.uncertainties:
                output += repr(i)
            output += "    value: " + str(self.x) + "\n"
        else:
            output = "  - value: " + str(self.x) + "\n"
        return output
        
class variable:
    #Variable class to contain the column header and each value the variable takes, with uncertainties
    def __init__(self, is_indep, header, qualifiers):
        self.is_indep = is_indep
        self.header = header
        self.qualifiers = qualifiers
        self.values = []
    def append(self, val):
        self.values.append(val)
    def __repr__(self):
        output = "- header: {" + self.header + "}\n"
        if self.qualifiers != "None":
            output += "  "
            qualifiers_file = open(self.qualifiers, "r")
            output += qualifiers_file.read()
            output += "\n"
        output += "  values:\n"
        for i in self.values:
            output += repr(i)
        return output

def discard(line):
    #Discards lines in tex file that begin with % or \, but not if it begins with \multiline
    line = line.strip(" \n\t~")
    if line == "":
        return True
    elif line.startswith("\multicolumn"):
        return False
    elif line[0] == "%" or line[0] == "\\":
        return True
    else:
        return False

def extract_header(string):
    #Extracts the name and units (if any) from the header of a column and returns a string suitable for yaml
    string = string.replace("\\xspace", "").strip(" \n\t~\\")
    if string.startswith("multicolumn"): #Removes leading multicolumn expression from header
        string = string[string.find("}")+1:]
        string = string[string.find("}")+1:]
    string.strip("{}")

    if "[" in string and "]" in string: #Takes expression in square brackets to be the units
        units = string[string.find("[")+1:string.find("]")]
        string = string[:string.find("[")]
        return "name: '" + string.strip(" \n\t~\\") + "', units: " + units.strip(" \n\t~\\")
    else:
        return "name: '" + string.strip(" \n\t~\\") + "'"

def split_format(string):
    temp_list = []
    for i in string.split(" "):
        if i == "i" or i == "d":
            temp_list.append([i])
        else:
            temp_list[-1].append(i)
    return temp_list

def extract_value(string, format_fragment): #Takes string with errors separated by commas and returns value object
    if string.replace("\t", "").replace(" ", "") == "":
        return value("'-'")
    else:
        temp_list = string.split("\pm")
        temp_value = value(temp_list[0].strip(" \n\t~\\$!"))
        for k in range(1,len(temp_list)):
            temp_value.append(uncertainty(temp_list[k].strip(" \n\t~\\$!"), format_fragment[k]))
        return temp_value

def write_variables(variable_list, file): #Writes variable objects in list to a yaml file
    file.write("dependent_variables:\n")
    """
    if args.qualifiers != "None":
        qualifiers_file = open(args.qualifiers, "r")
        output_file.write(qualifiers_file.read())
        output_file.write("\n")
    """
    for i in variable_list:
        if i.is_indep == False:
            file.write(repr(i))
    file.write("independent_variables:\n")
    for i in variable_list:
        if i.is_indep == True:
            file.write(repr(i))

parser = argparse.ArgumentParser()
parser.add_argument("-file", type=str)
parser.add_argument("-format", type=str)
parser.add_argument("--covariance", action="store_true", default=False)
parser.add_argument("-qualifiers", type=str, default="None")

args = parser.parse_args()

"""
input_file = open("table.tex", "r") #####
covariance = False #####
column = "ideeedeee" #####
"""

input_file = open(args.file, "r")
lines = input_file.readlines() #Stores each line of the file as a string in in the lines list
input_file.close()

data_array = []
for i in lines:
    if discard(i) == False:
        data_array.append(i.replace("\\hline", "").strip(" \n\t~\\").replace("&$\pm$&", "\pm").replace("&\pm&", "\pm").split("&"))
data_array = np.array(data_array)

if args.covariance == True:
    indep_header = extract_header(data_array[0,0])
    variables = [variable(True, indep_header, "None"), variable(True, indep_header, "None"), variable(False, "name: Correlation", args.qualifiers)]
    x_axis = []
    y_axis = []
    for i in data_array[0, 1:]:
        x_axis.append(extract_value(i, ["i"]))
    for i in data_array[1:, 0]:
        y_axis.append(extract_value(i, ["i"]))
    for i in range(len(x_axis)):
        for j in range(len(y_axis)):
            variables[0].append(x_axis[j])
            variables[1].append(y_axis[i])

    dep_values = data_array[1:,1:].flatten()
    for i in dep_values:
        variables[2].append(extract_value(i, ["d"]))

else:
    variables = []
    format_list = split_format(args.format)
    for i in range(len(format_list)):
        if format_list[i][0] == "i":
            variables.append(variable(True, extract_header(data_array[0,i]), "None"))
        elif format_list[i][0] == "d":
            variables.append(variable(False, extract_header(data_array[0,i]), args.qualifiers))
    for i in range(1,len(data_array)):
        for j in range(len(data_array[i,:])):
            variables[j].append(extract_value(data_array[i,j], format_list[j]))
            
output_file = open(args.file.replace(".tex", ".yaml"), "w")
write_variables(variables, output_file)
output_file.close()   
